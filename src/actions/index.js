export const add_movie = (movieData) => {
   return {
      type: 'ADD_MOVIE',
      payload: movieData
   }
}

export const delete_movie = id => {
   return {
      type: 'DELETE_MOVIE',
      id
   }
}