import setMoviesReducer from "./setMoviesReducer";
import {add_movie, delete_movie} from "../actions";

let state = {
   movieData: [
      {
         id: 1,
         title: 'Terminator 2',
         image: 'https://upload.wikimedia.org/wikipedia/en/8/85/Terminator2poster.jpg',
         description: 'Terminator 2: Judgment Day was released in the United States on July 3, 1991 by TriStar Pictures. It was a critical success upon its release, with praise going towards the performances of its cast, the action scenes, and its visual effects.',
         trailer: 'https://www.youtube.com/watch?v=CRRlbK5w8AE'
      },
   ]
};

it ('length of movieData array should be incremented', () => {
   // 1. test data
   let action = add_movie({
      id: 2,
      title: 'Title to test',
      image: 'https://image.shutterstock.com/image-vector/web-development-illustration-computer-display-600w-387511441.jpg',
      description: 'Unit test description',
      trailer: 'https://www.youtube.com/'
   });
   // 2. action
   let newState = setMoviesReducer(state, action);
   // 3. expectation
   expect(newState.movieData.length).toBe(2);
})


it ('title of new movie in array should be correct', () => {
   // 1. test data
   let action = add_movie({
      id: 2,
      title: 'Title to test',
      image: 'https://image.shutterstock.com/image-vector/web-development-illustration-computer-display-600w-387511441.jpg',
      description: 'Unit test description',
      trailer: 'https://www.youtube.com/'
   });
   // 2. action
   let newState = setMoviesReducer(state, action);
   // 3. expectation
   expect(newState.movieData[1].title).toBe('Title to test');
})


it ('after deleting length of items in movieData array should be decremented', () => {
   // 1. test data
   let action = delete_movie(1);
   // 2. action
   let newState = setMoviesReducer(state, action);
   // 3. expectation
   expect(newState.movieData.length).toBe(0);
})


it (`after deleting length shouldn't be decremented if id is incorrect`, () => {
   // 1. test data
   let action = delete_movie(9);
   // 2. action
   let newState = setMoviesReducer(state, action);
   // 3. expectation
   expect(newState.movieData.length).toBe(1);
})