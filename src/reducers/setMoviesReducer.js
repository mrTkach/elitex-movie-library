let initialState = {
   movieData: [
      {
         id: 1,
         title: 'Terminator 2',
         image: 'https://upload.wikimedia.org/wikipedia/en/8/85/Terminator2poster.jpg',
         description: 'Terminator 2: Judgment Day was released in the United States on July 3, 1991 by TriStar Pictures. It was a critical success upon its release, with praise going towards the performances of its cast, the action scenes, and its visual effects.',
         trailer: 'https://www.youtube.com/watch?v=CRRlbK5w8AE'
      },
      {
         id: 2,
         title: 'The Thirteenth Floor',
         image: 'https://upload.wikimedia.org/wikipedia/en/thumb/0/02/The_Thirteenth_Floor_poster.jpg/220px-The_Thirteenth_Floor_poster.jpg',
         description: 'The Thirteenth Floor is a 1999 science fiction neo-noir film written and directed by Josef Rusnak, and produced by Roland Emmerich. It is loosely based upon Simulacron-3 (1964), a novel by Daniel F. Galouye, and a remake of the German film World on a Wire (1973).',
         trailer: 'https://www.youtube.com/watch?v=dtYdZkPmFoU'
      },
      {
         id: 3,
         title: 'World War Z',
         image: 'https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcSKyUCNCa692Vf7Gp229VCEObviKZladaLDcaohN87LCpTm77T1',
         description: 'World War Z is a 2013 American post-apocalyptic zombie action horror film directed by Marc Forster, with a screenplay by Matthew Michael Carnahan, Drew Goddard, and Damon Lindelof, from a story by Carnahan and J. Michael Straczynski, based on the 2006 novel of the same name by Max Brooks.',
         trailer: 'https://www.youtube.com/watch?v=Md6Dvxdr0AQ'
      },
   ]
};

export const ADD_MOVIE = 'ADD_MOVIE';
export const DELETE_MOVIE = 'DELETE_MOVIE';

const setMoviesReducer = (state = initialState, action) => {
   switch(action.type){
      case ADD_MOVIE:
         return {
            ...state,
             movieData: [...state.movieData, action.payload]
         };
      case DELETE_MOVIE:
         return {
            ...state,
            movieData: state.movieData.filter(item => item.id !== action.id)
         }
      default:
         return state;
   }
}

export default setMoviesReducer;
