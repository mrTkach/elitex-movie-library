import {combineReducers} from "redux";

import setMoviesReducer from './setMoviesReducer';

const reducers = combineReducers({
   state: setMoviesReducer
})

export default reducers;
