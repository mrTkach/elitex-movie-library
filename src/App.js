import React, {useState} from 'react';
import {useSelector} from 'react-redux';
import {Button, Col, Container, Row} from "react-bootstrap";
import Logo from './assets/images/logo.svg';

import MovieCard from "./components/MovieCard";
import ModalForm from "./components/ModalForm";

const App = () => {

    const moviesList = useSelector(state => state.state.movieData);
    const [showForm, setShowForm] = useState(false);
    const handleShow = () => setShowForm(true);
    const handleClose = () => setShowForm(false);

    return (
        <Container className="mb-4">
            {showForm && <ModalForm showForm={showForm} handleClose={handleClose} />}
            <Row style={{padding: '15px 0'}} className="align-items-center">
               <Col>
                   <a href="/">
                       <img src={Logo} alt="logo" width='150' />
                   </a>
               </Col>
            </Row>
            <Row>
                <Col>
                    <h1 className="text-center">Elitex Movie Library</h1>
                </Col>
            </Row>
            <Row>
                <Col sm={12} md={9}>
                    <p>Welcome to our movie library!</p>
                    <p>Here you can find movie recommendations from our Elitex colleagues that are nice to watch.</p>
                    <p>Feel free to add your suggestions for Elitex team with films that have impressed you.</p>
                </Col>
                <Col sm={12} md={3} className="text-right mb-3">
                    <Button variant="success" onClick={() => handleShow(true)}>
                        Add new film
                    </Button>
                </Col>
            </Row>

            <Row>
                {moviesList.length
                    ? moviesList.map(movie => (
                        <MovieCard key={movie.id} movie={movie} moviesList={moviesList} />))
                    : (
                        <Col>
                            <div>There are no recommendations of movies to watch. You can be the first.</div>
                            <div>What would you suggest?</div>
                        </Col>
                    )
                }
            </Row>
        </Container>
    );
}

export default App;
