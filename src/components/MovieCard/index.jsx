import React, {useState} from 'react';
import {delete_movie} from "../../actions";
import {useDispatch} from "react-redux";
import PropTypes from 'prop-types';
import {Button, Card, Col} from "react-bootstrap";
import styles from "./styles.module.css";


const MovieCard = ({ movie }) => {

    const dispatch = useDispatch();

    const [rolled, setRolled] = useState(true);

    const showMore = () => {
        rolled ? setRolled(false) : setRolled(true)
    }

    const style = {
        height: rolled ? '45px' : '150px'
    }

    const deleteHandle = id => {
        dispatch(delete_movie(id));
    }

    return (
        <Col xs={12} sm={6} md={4}>
            <Card className={styles.movieCard}>
                <Card.Img variant="top" src={movie && movie.image} />
                <Card.Body>
                    <Card.Title>{movie && movie.title}</Card.Title>
                    {movie && movie.description &&
                        <Card.Text>
                            <div className={styles.rolledText} style={style}>
                                {movie.description}
                            </div>
                            <div onClick={() => showMore()} className={styles.rolledLink}>
                                {rolled ? 'See more' : 'See less'}
                            </div>
                        </Card.Text>
                    }
                    <div className="d-flex justify-content-between" >
                        <Button href={movie && movie.trailer} target="_blank" variant="info">Trailer</Button>
                        <Button onClick={()=>deleteHandle(movie.id)} variant="danger">Delete</Button>
                    </div>
                </Card.Body>
            </Card>
        </Col>
    );
};

MovieCard.propTypes = {
    movie: PropTypes.shape({
        title: PropTypes.string,
        image: PropTypes.string,
        description: PropTypes.string,
        trailer: PropTypes.string
    })
};

export default MovieCard;
