import React, {useState} from 'react';
import {useDispatch} from "react-redux";
import {add_movie} from "../../actions";
import {Button, Modal, Form} from "react-bootstrap";
import PropTypes from 'prop-types';

const ModalForm = ({ showForm, handleClose }) => {

    const [newMovie, setNewMovie] = useState({
        title: '',
        image: '',
        description: '',
        trailer: ''
    });

    const [validated, setValidated] = useState(false);

    const dispatch = useDispatch();

    const addHandle = (e) => {
        e.preventDefault();
        let movie = {
            id: Date.now().toString(),
            title: newMovie.title,
            image: newMovie.image,
            description: newMovie.description,
            trailer: newMovie.trailer
        }
        dispatch(add_movie(movie));
    }

    const changeHandle = e => {
        const {name, value} = e.target;
        setNewMovie(prevValue => {
            return {
                ...prevValue,
                [name]: value
            }
        })
    }

    const submitHandle = (event) => {
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        } else {
            addHandle(event);
            handleClose(false);
        }
        setValidated(true);
    }


    return (
        <Modal show={showForm} onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>Modal heading</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form noValidate validated={validated} onSubmit={submitHandle}>
                    <Form.Group controlId="title">
                        <Form.Label>Add title of movie</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Title"
                            autoComplete="off"
                            value={newMovie.title}
                            name="title"
                            required
                            onChange={e => changeHandle(e)}
                        />
                    </Form.Group>
                    <Form.Group controlId="preview">
                        <Form.Label>Paste link to image preview</Form.Label>
                        <Form.Control
                            type="url"
                            placeholder="https://..."
                            autoComplete="off"
                            value={newMovie.image}
                            name="image"
                            required
                            onChange={e => changeHandle(e)}
                        />
                    </Form.Group>
                    <Form.Group controlId="description">
                        <Form.Label>Add short description</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Description"
                            autoComplete="off"
                            value={newMovie.description}
                            name="description"
                            onChange={e => changeHandle(e)}
                        />
                    </Form.Group>
                    <Form.Group controlId="trailer">
                        <Form.Label>Paste a link to the trailer on Youtube</Form.Label>
                        <Form.Control
                            type="url"
                            placeholder="https://..."
                            autoComplete="off"
                            value={newMovie.trailer}
                            name="trailer"
                            required
                            onChange={e => changeHandle(e)}
                        />
                    </Form.Group>
                    <Button type="submit">Add</Button>
                </Form>
            </Modal.Body>
            <Modal.Footer>

            </Modal.Footer>
        </Modal>
    );
};

ModalForm.propTypes = {
    showForm: PropTypes.bool,
    setShowForm: PropTypes.func
}

export default ModalForm;